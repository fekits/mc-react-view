# MC-REACT-VIEW
```$xslt

视图控制器

目的：为了使组件保持干净并专注于内容本身，将组件显示隐藏等操作从组件文件中抽象出来。从而减少每个组件的冗余代码与操作。

1、组件载入时"created"，为组件添加视图控制器方法，此方法将自动为组件挂载视图显示、隐藏与监听状态变化的回调事件。流程请看以下示例：

   A组件载入
   ↓
   为A组件添加视图控制器
   ↓
   A.show()  A.hide()  A.event() ...   视图控制器为组件挂载了显示隐藏与组件内的事件回调等方法
   ↓
   将A组件挂载到全局事件总线this.$events
   ↓
   在全局任意地方可以使用全局事件总线访问A组件及其属性与方法
   ↓
   例如：
   this.$events.A.show() 显示A组件
   ↓
   例如：
   this.$events.A.hide() 隐藏A组件
   ↓
   并且在组件的显示或隐藏事件中都可以访问到组件的事件回调
   ↓
   例如：
   React.$Bus.A.show({
     on:{
       show($this){
         // 当A组件显示时做些事情...
         // 并且可以访问到A组件本身_this
       },
       hide($this){
         // 当A组件隐藏时做些事情...
       },
       event(name){
         // A组件内暴露的一些事件...

         // 比如执行A组件内名字为login的事件
         if(name==='login'){
           // 去登录...
         }
       }
     }
   })

1、词汇说明：

   状态码：

   状态码有哪些？
   状态码"status"目前有0=删除，1=隐藏，2=显示

   状态码为什么可以控制组件显示？
   在组件的总容器标签上写上 v-if="status" 和view="status"，当"status"=0时相当于v-if=，0就是false，此时组件就删除了。而当status大于0时，v-if相法于为true,就是显示状态；
   而view=1时。组件有配套的CSS中写了当[view="1"]时开始播放一个隐藏的animation动画，同样的当[view="2"]时播放一个显示动画

   显示
   显示包含了一个从隐藏到显示的过程动画，当view=2时，触发一个显示出来的动画直到动画结束保持最后的显示状态

   隐藏
   隐藏包含了一个从显示到隐藏的过程动画和最后的隐藏状态，此时组件DOM还存在于文档中。

   删除
   如果组件直接从DOM删除，则无法播放隐藏动画，所以，在隐藏动画结束时，再将组件的DOM节点从文档中删除

```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
*


#### 开始

下载项目: 
```npm
npm i mc-react-view
```

#### 参数
```$xslt
*
```

#### 示例

```javascript
// 为级件添加事件总线和视图控制器
constructor(props) {
    ...
    // 事件总线+视图控制器
    React.Bus.Load = React.view(this);
    ...
}

// 在项目的任意组件中使用
React.Bus.Load.show();

// 一个复杂的示例
React.Bus.Load.show({
  // 在显示前顺便传入一些数据
  state:{
    aaa:'this is aaa',
    bbb:'this is bbb'
  },
  // 顺便发一个埋点
  track:{
    id: 'xxx',
    param: 'xxx'
  },
  // 顺便监听一堆回调
  on:{
    show(){
      // 组件显示后执行  
    },
    hide(){
      // 组件隐藏后执行  
    },
    none(){
      // 组件移除后执行  
    },
    xxx(){
      // 其它组件内爆露的  
    }   
  }
});


React.Bus.Load.hide();
React.Bus.Load.none();
React.Bus.Load.when({
  event1(){
    console.log('组件内向外发送的事件之一');
  }
});
```

#### 版本

```$xslt
v1.0.5 [Last Version]
1. 绑定监听动画事件的DOM不再需要手工写入ref='anim';而是在mc-react-view中从React内部属_reactInternalFiber.alternate.child.stateNode找到真实节点。我不确定这是否可靠，但是我仍想试试。
```

```$xslt
v1.0.3 [Last Version]
1. 添加一个when用于监听与接收组件内向外发送的事件
```

```$xslt
v1.0.0
1. 核心功能完成。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
