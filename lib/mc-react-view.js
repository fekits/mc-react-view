import React from 'react';

/**
 * 视图控制器
 *
 * 目的：为了使组件保持干净并专注于内容本身，将组件显示隐藏等操作从组件文件中抽象出来。从而减少每个组件的冗余代码与操作。
 *
 * 1、组件载入时"constructor"，为组件添加视图控制器方法，此方法将自动为组件挂载视图显示、隐藏与监听状态变化的回调事件。流程请看以下示例：
 *
 *    A组件载入
 *    ↓
 *    为A组件添加视图控制器
 *    ↓
 *    A.show()  A.hide()  A.event() ...   视图控制器为组件挂载了显示隐藏与组件内的事件回调等方法
 *    ↓
 *    将A组件挂载到全局事件总线React.Bus
 *    ↓
 *    在全局任意地方可以使用全局事件总线访问A组件及其属性与方法
 *    ↓
 *    例如：
 *    React.Bus.A.show() 显示A组件
 *    ↓
 *    例如：
 *    React.Bus.A.hide() 隐藏A组件
 *    ↓
 *    并且在组件的显示或隐藏事件中都可以访问到组件的事件回调
 *    ↓
 *    例如：
 *    React.Bus.A.show({
 *      on:{
 *        show($this){
 *          // 当A组件显示时做些事情...
 *          // 并且可以访问到A组件本身_this
 *        },
 *        hide($this){
 *          // 当A组件隐藏时做些事情...
 *        },
 *        event(name){
 *          // A组件内暴露的一些事件...
 *
 *          // 比如执行A组件内名字为login的事件
 *          if(name==='login'){
 *            // 去登录...
 *          }
 *        }
 *      }
 *    })
 *
 * 1、词汇说明：
 *
 *    状态码：
 *
 *    状态码有哪些？
 *    状态码"status"目前有0=删除，1=隐藏，2=显示
 *
 *    状态码为什么可以控制组件显示？
 *    在组件的总容器标签上写上 this.state.status?(...):null 和view="status"，当"status"=0时，0相当于是false，此时组件就删除了。而当status大于0时，v-if相法于为true,就是显示状态；
 *    而view=1时。组件有配套的CSS中写了当[view="1"]时开始播放一个隐藏的animation动画，同样的当[view="2"]时播放一个显示动画
 *
 *    显示
 *    显示包含了一个从隐藏到显示的过程动画，当view=2时，触发一个显示出来的动画直到动画结束保持最后的显示状态
 *
 *    隐藏
 *    当view=1时，触发个配套的CSS从显示到隐藏的过程动画和最后的隐藏状态，此时组件DOM还存在于文档中。
 *
 *    删除
 *    如果组件直接从DOM删除，则无法播放隐藏动画，所以，在隐藏动画结束时，再将组件的DOM节点从文档中删除
 * */

// 动画事件兼容处理
let amEnd = (function () {
  let evs = {
    WebkitAnimation: 'webkitAnimationEnd',
    animation: 'animationend'
  };
  for (let k in evs) {
    if (typeof evs[k] === 'string') {
      return evs[k];
    }
  }
})();

let McReactView = () => {

  React.view = ($this) => {
    // 重置回调
    $this.on = {};

    // 前置工作
    let prep = (param) => {
      // 添加数据
      if (param.state) {
        for (let key in param.state) {
          if (param.state.hasOwnProperty(key)) {
            $this.setState({ key: param.state[key] });
          }
        }
      }

      // 发送埋点
      if (param.track) {
        React.track.ev(param.track);
      }

      // 回调集合
      Object.assign($this.on, param.on || {});
    };

    let stateNode = () => {
      let a = $this._reactInternalFiber;
      if (a) {
        a = a.alternate;
        if (a) {
          a = a.child;
          if (a) {
            return a.stateNode;
          }
        }
      }

    };

    // 显示组件
    $this.show = (param = {}) => {

      // 前置工作
      prep(param);

      // 隐藏组件
      let animDom = $this.refs.anim || stateNode();
      if (animDom) {

        // 动画播放结束
        let amend = () => {
          if ($this.state.status === 2) {
            animDom.removeEventListener(amEnd, amend, false);
            $this.on.show && $this.on.show();
          }
        };
        // 监听动画事件
        animDom.addEventListener(amEnd, amend, false);

        // 开始隐藏动画
        $this.setState({ status: 2 });
      } else {
        // 直接移除组件
        $this.setState({ status: 2 }, () => {
          $this.on.show && $this.on.show();
        });
      }
    };

    // 隐藏组件
    $this.hide = (param = {}) => {

      // 前置工作
      prep(param);

      // 隐藏组件
      let animDom = $this.refs.anim || stateNode();
      if (animDom) {

        // 动画播放结束
        let amend = () => {
          if ($this.state.status === 1) {
            animDom.removeEventListener(amEnd, amend, false);
            $this.setState({ status: 0 }, () => {
              $this.on.hide && $this.on.hide();
            });
          }
        };
        // 监听动画事件
        animDom.addEventListener(amEnd, amend, false);

        // 开始隐藏动画
        $this.setState({ status: 1 });
      } else {
        // 直接移除组件
        $this.setState({ status: 0 }, () => {
          $this.on.hide && $this.on.hide();
        });
      }

    };

    // 卸载组件
    $this.none = (param = {}) => {
      // 前置工作
      prep(param);

      // 卸载组件
      $this.setState({ status: 0 }, () => {
        $this.on.none && $this.on.none();
      });
    };

    $this.when = (param = {}) => {
      // 回调集合
      Object.assign($this.on, param || {});
    };

    return $this;
  };
};

export default McReactView;
